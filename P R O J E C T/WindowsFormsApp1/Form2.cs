﻿using NewGift;
using System;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class AddCandy : Form
    {
        private DialogResult _result;
        public DialogResult Result
        {
            get { return _result; }
            set { _result = value; }
        }
        private Candy _abstractCandy;
        public Candy ACandy
        {
            get { return _abstractCandy; }
            set { _abstractCandy = value; }
        }
        public MainForm MF { get; set; }
        public AddCandy()
        {
            InitializeComponent();
            typeCandies.Items.AddRange(new String[] { "Chocolate", "Jelly", "Lolipop", "Pastille" });
            typeCandies.Show();
            
            MainForm MF = (MainForm)this.Owner;
        }
        
        //Применение свойств
        public void Set(Candy candy)
        {
            candy.Name = nameBox.Text;
            candy.Weight = Convert.ToDouble(weightBox.Text);
            candy.Quantity = Convert.ToInt32(quantityBox.Text);
            candy.Taste = tasteBox.Text;
        }
        public void ApplyJelly(Jelly jelly)
        {
            Set(jelly);
            jelly.Shape = dopProp1.Text;
            return;
        }
        public void ApplyChoco(Chocolate chocolate)
        {
            Set(chocolate);
            chocolate.Bitterness = Convert.ToDouble(dopProp1.Text);
            return;
        }
        public void ApplyLolipop(Lolipop lolipop)
        {
            Set(lolipop);
            lolipop.Colour = dopProp1.Text;
            lolipop.HasFilling = dopProp2.Text;
            return;
        }
        public void ApplyPastille(Pastille monpas)
        {
            ApplyLolipop(monpas);
            monpas.Smell = dopProp3.Text;
        }
        //Отчистка и сокрытие дополнительных полей
        public void ClearDopFields()
        {          
            label5.Text = "";
            label6.Text = "";
            label7.Text = "";
            dopProp1.Visible = false;
            dopProp2.Visible = false;
            dopProp3.Visible = false;
            dopProp1.Enabled = false;
            dopProp2.Enabled = false;
            dopProp3.Enabled = false;
            return;
        }
        public void EnableMainFields()
        {
            nameBox.Enabled = true;
            weightBox.Enabled = true;
            quantityBox.Enabled = true;
            tasteBox.Enabled = true;
        }
        //Добавление дополнительных полей в зависимости от выбора вида конфетки:
        private void typeCandies_SelectedIndexChanged(object sender, EventArgs e) 
        {
            int id = typeCandies.SelectedIndex;
            switch (id)
            {
                case 0:
                    EnableMainFields();
                    ClearDopFields();
                    label5.Text = "Bitterness:";
                    dopProp1.Enabled = true;
                    dopProp1.Visible = true;
                    break;
                case 1:
                    EnableMainFields();
                    ClearDopFields();
                    label5.Text = "Shape:";
                    dopProp1.Enabled = true;
                    dopProp1.Visible = true;
                    break;
                case 2:
                    EnableMainFields();
                    ClearDopFields();
                    label5.Text = "Colour";
                    dopProp1.Enabled = true;
                    dopProp1.Visible = true;
                    label6.Text = "Has filling?:";
                    dopProp2.Enabled = true;
                    dopProp2.Visible = true;
                    break;
                case 3:
                    EnableMainFields();
                    ClearDopFields();
                    label5.Text = "Colour";
                    dopProp1.Enabled = true;
                    dopProp1.Visible = true;
                    label6.Text = "Has filling?:";
                    dopProp2.Enabled = true;
                    dopProp2.Visible = true;
                    label7.Text = "Smell";
                    dopProp3.Enabled = true;
                    dopProp3.Visible = true;
                    break;
                default:
                    break;
            }
        }
        
        private void applyButton_Click(object sender, EventArgs e)
        {
            if (nameBox.Text == "" | quantityBox.Text == "" | weightBox.Text == "" | tasteBox.Text == "" | dopProp1.Text == "")
            {
                MessageBox.Show("all fields must be filled", "error", MessageBoxButtons.OK);
                return;
            }
            int id = typeCandies.SelectedIndex;
            //трансформация значений полей в данные конфетки
            switch (id)
            {
                case 0:
                    Chocolate choco = new Chocolate();
                    ApplyChoco(choco);
                    ACandy = choco;
                    break;
                case 1:
                    Jelly jelly = new Jelly();
                    ApplyJelly(jelly);
                    ACandy = jelly;
                    break;
                case 2:
                    Lolipop loli = new Lolipop();
                    ApplyLolipop(loli);
                    ACandy = loli;
                    break;
                case 3:
                    Pastille monpas = new Pastille();
                    ApplyPastille(monpas);
                    ACandy = monpas;
                    break;
            }

            Result = DialogResult.OK;
            this.Hide();
            return;
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            Result = DialogResult.Cancel;
            this.Hide();
            return;
        }
        double i = ',';
        private void weightBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsDigit(e.KeyChar) && (e.KeyChar != 44 && e.KeyChar != 8 && (e.KeyChar < 48 || e.KeyChar > 57)))
                e.Handled = true;
        }

        private void quantityBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsDigit(e.KeyChar) && (e.KeyChar != 8 && (e.KeyChar < 48 || e.KeyChar > 57)))
                e.Handled = true;
        }

        private void dopProp1_KeyPress(object sender, KeyPressEventArgs e)
        {
            int id = typeCandies.SelectedIndex;

            if (!char.IsDigit(e.KeyChar) && (e.KeyChar != 44 && e.KeyChar != 8 && (e.KeyChar < 48 || e.KeyChar > 57) ) )
                e.Handled = true;
        }
    }
    
}
