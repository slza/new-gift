﻿using System;
using NewGift;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class MainForm : Form
    {
        public void DeleteCandy(Candy candy)
        {
            listCandies.Items.Remove(listCandies.Items[select]);
            addCandies.Remove(addCandies[select]);
            gift.ListCandies.Remove(candy);
            clickAdd = gift.ListCandies.Count;
            select = clickAdd;
            return;
        }
        int clickAdd = 0;
        int select = 0;
        List<AddCandy> addCandies = new List<AddCandy>();
        Candy gift = new Candy();
        public MainForm()
        {
            InitializeComponent();
        }
        private void addButton_Click(object sender, EventArgs e)
        {
            select = clickAdd;
            addCandies.Add(new AddCandy());
            addCandies[clickAdd].ShowDialog();
        }
        
        private void editButton_Click(object sender, EventArgs e)
        {
            select = listCandies.SelectedIndex;
            if (select>=0 && select<clickAdd)
                addCandies[select].ShowDialog();
        }

        public void FormRefresh()
        {
            for(int j = 0; j < gift.ListCandies.Count; j++)
            {
                listCandies.Items[j] = (j + 1).ToString() + ". " + gift.ListCandies[j].Name + "[" + gift.ListCandies[j].Weight.ToString() + "g] – " + gift.ListCandies[j].Quantity.ToString() + "pcs.";
            }
            totalWeight.Text = gift.CountWeight().ToString() + " g";
        }
        private void MainForm_Activated(object sender, EventArgs e)
        {
            if (addCandies.Count() != 0)
                if (addCandies[select].Result == DialogResult.OK)
                {
                    if (select < clickAdd) //выполняется, если форма addCandy вызвана через кнопку Edit
                    {
                        gift.ListCandies[select] = addCandies[select].ACandy; //меняет конфигурацию конфетки
                    }
                    else
                    {
                        gift.AddCandy(addCandies[clickAdd].ACandy);
                        listCandies.Items.Add("");
                        clickAdd++;
                    } //выполняется, если форма addCandy вызвана через кнопку Add  
                    FormRefresh();
                }
        }

        private void deleteButton_Click(object sender, EventArgs e)
        {
            select = listCandies.SelectedIndex;
            if(select!=-1)
                DeleteCandy(gift.ListCandies[select]);
            FormRefresh();
        }
    }
    }

