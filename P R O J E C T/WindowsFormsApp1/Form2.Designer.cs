﻿namespace WindowsFormsApp1
{
    partial class AddCandy
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.nameBox = new System.Windows.Forms.TextBox();
            this.weightBox = new System.Windows.Forms.TextBox();
            this.quantityBox = new System.Windows.Forms.TextBox();
            this.tasteBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.dopProp1 = new System.Windows.Forms.TextBox();
            this.dopProp2 = new System.Windows.Forms.TextBox();
            this.dopProp3 = new System.Windows.Forms.TextBox();
            this.applyButton = new System.Windows.Forms.Button();
            this.cancelButton = new System.Windows.Forms.Button();
            this.typeCandies = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // nameBox
            // 
            this.nameBox.Enabled = false;
            this.nameBox.Location = new System.Drawing.Point(101, 50);
            this.nameBox.Name = "nameBox";
            this.nameBox.Size = new System.Drawing.Size(139, 20);
            this.nameBox.TabIndex = 1;
            // 
            // weightBox
            // 
            this.weightBox.Enabled = false;
            this.weightBox.ImeMode = System.Windows.Forms.ImeMode.Alpha;
            this.weightBox.Location = new System.Drawing.Point(101, 76);
            this.weightBox.Name = "weightBox";
            this.weightBox.Size = new System.Drawing.Size(137, 20);
            this.weightBox.TabIndex = 2;
            this.weightBox.Text = "\r\n";
            this.weightBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.weightBox_KeyPress);
            // 
            // quantityBox
            // 
            this.quantityBox.Enabled = false;
            this.quantityBox.Location = new System.Drawing.Point(101, 102);
            this.quantityBox.Name = "quantityBox";
            this.quantityBox.Size = new System.Drawing.Size(139, 20);
            this.quantityBox.TabIndex = 3;
            this.quantityBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.quantityBox_KeyPress);
            // 
            // tasteBox
            // 
            this.tasteBox.Enabled = false;
            this.tasteBox.Location = new System.Drawing.Point(101, 128);
            this.tasteBox.Name = "tasteBox";
            this.tasteBox.Size = new System.Drawing.Size(139, 20);
            this.tasteBox.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 53);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Name:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 79);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(90, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Weight (per item):";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 105);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(49, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Quantity:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 131);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(37, 13);
            this.label4.TabIndex = 5;
            this.label4.Text = "Taste:";
            // 
            // dopProp1
            // 
            this.dopProp1.Enabled = false;
            this.dopProp1.Location = new System.Drawing.Point(101, 154);
            this.dopProp1.Name = "dopProp1";
            this.dopProp1.Size = new System.Drawing.Size(139, 20);
            this.dopProp1.TabIndex = 4;
            this.dopProp1.Visible = false;
            this.dopProp1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.dopProp1_KeyPress);
            // 
            // dopProp2
            // 
            this.dopProp2.Enabled = false;
            this.dopProp2.Location = new System.Drawing.Point(101, 180);
            this.dopProp2.Name = "dopProp2";
            this.dopProp2.Size = new System.Drawing.Size(139, 20);
            this.dopProp2.TabIndex = 4;
            this.dopProp2.Visible = false;
            // 
            // dopProp3
            // 
            this.dopProp3.Enabled = false;
            this.dopProp3.Location = new System.Drawing.Point(101, 206);
            this.dopProp3.Name = "dopProp3";
            this.dopProp3.Size = new System.Drawing.Size(139, 20);
            this.dopProp3.TabIndex = 4;
            this.dopProp3.Visible = false;
            // 
            // applyButton
            // 
            this.applyButton.Location = new System.Drawing.Point(12, 240);
            this.applyButton.Name = "applyButton";
            this.applyButton.Size = new System.Drawing.Size(75, 23);
            this.applyButton.TabIndex = 6;
            this.applyButton.Text = "OK";
            this.applyButton.UseVisualStyleBackColor = true;
            this.applyButton.Click += new System.EventHandler(this.applyButton_Click);
            // 
            // cancelButton
            // 
            this.cancelButton.Location = new System.Drawing.Point(164, 240);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(75, 23);
            this.cancelButton.TabIndex = 6;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // typeCandies
            // 
            this.typeCandies.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.typeCandies.FormattingEnabled = true;
            this.typeCandies.Location = new System.Drawing.Point(15, 12);
            this.typeCandies.Name = "typeCandies";
            this.typeCandies.Size = new System.Drawing.Size(225, 21);
            this.typeCandies.TabIndex = 7;
            this.typeCandies.SelectedIndexChanged += new System.EventHandler(this.typeCandies_SelectedIndexChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 157);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(0, 13);
            this.label5.TabIndex = 8;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 183);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(0, 13);
            this.label6.TabIndex = 8;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(12, 209);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(0, 13);
            this.label7.TabIndex = 8;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.GhostWhite;
            this.label8.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label8.Location = new System.Drawing.Point(223, 80);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(13, 13);
            this.label8.TabIndex = 9;
            this.label8.Text = "g";
            // 
            // AddCandy
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackColor = System.Drawing.Color.SlateBlue;
            this.ClientSize = new System.Drawing.Size(251, 275);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.typeCandies);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.applyButton);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dopProp3);
            this.Controls.Add(this.dopProp2);
            this.Controls.Add(this.dopProp1);
            this.Controls.Add(this.tasteBox);
            this.Controls.Add(this.quantityBox);
            this.Controls.Add(this.weightBox);
            this.Controls.Add(this.nameBox);
            this.MaximizeBox = false;
            this.Name = "AddCandy";
            this.Text = "Candy Properities";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TextBox nameBox;
        private System.Windows.Forms.TextBox weightBox;
        private System.Windows.Forms.TextBox quantityBox;
        private System.Windows.Forms.TextBox tasteBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox dopProp1;
        private System.Windows.Forms.TextBox dopProp2;
        private System.Windows.Forms.TextBox dopProp3;
        private System.Windows.Forms.Button applyButton;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.ComboBox typeCandies;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
    }
}